@extends("front.master")

@section("title")
    E-Medicine
    @endsection

@section("body")
    <div class="contentText">
        <div class="breadcrumbs">
            <a href="{{ url('/home') }}" class="headerNavigation"><i class="fa fa-home"></i></a>
        </div>
    </div>

    <!----slidder start-!-->
    <div class="contentText">
        <div class="infoBoxHeading">Today Bestsellers</div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12"  >
                <!--                                        <div class="bg_best">-->
                <div class="bg_best">
                    <div class="owl-carousel">
                        <div class="item">
                                                        <span>
                                                            <a href="{{ url('/view-product') }}">
                                                                <img class="carasoul_image" src="{{ asset('/') }}front/images/d1.jpg">
                                                            </a></span>
                            <a class="btn btn-default"  href="{{ url('/cart') }}" role="button" >
                                Buy Now!
                            </a>
                        </div>
                        <div class="item">
                                                        <span>
                                                            <a href="{{ url('/view-product') }}">
                                                                <img class="carasoul_image" src="{{ asset('/') }}front/images/img4.jpg">
                                                            </a>
                                                        </span>
                            <a class="btn btn-default"  href="{{ url('/cart') }}" role="button" >
                                Buy Now!
                            </a>
                        </div>
                        <div class="item">
                                                        <span>
                                                            <a href="{{ url('/view-product') }}">
                                                                <img class="carasoul_image" src="{{ asset('/') }}front/images/img6.jpg">
                                                            </a></span>
                            <a class="btn btn-default"  href="{{ url('/cart') }}" role="button" >
                                Buy Now!
                            </a>
                        </div>
                        <div class="item">
                                                        <span>
                                                            <a href="{{ url('/view-product') }}">
                                                                <img class="carasoul_image" src="{{ asset('/') }}front/images/img13.jpg">
                                                            </a></span>
                            <a class="btn btn-default"  href="{{ url('/cart') }}" role="button" >
                                Buy Now!
                            </a>
                        </div>
                        <div class="item">
                                                        <span>
                                                            <a href="{{ url('/view-product') }}">
                                                                <img class="carasoul_image" src="{{ asset('/') }}front/images/img14.jpg">
                                                            </a></span>
                            <a class="btn btn-default"  href="{{ url('/cart') }}" role="button" >
                                Buy Now!
                            </a>
                        </div>
                        <div class="item">
                                                        <span>
                                                            <a href="{{ url('/view-product') }}">
                                                                <img class="carasoul_image" src="{{ asset('/') }}front/images/img16.jpg">
                                                            </a></span>
                            <a class="btn btn-default"  href="{{ url('/cart') }}" role="button" >
                                Buy Now!
                            </a>
                        </div>
                        <div class="item">
                                                        <span>
                                                            <a href="{{ url('/view-product') }}">
                                                                <img class="carasoul_image" src="{{ asset('/') }}front/images/img15.jpg">
                                                            </a></span>
                            <a class="btn btn-default"  href="{{ url('/cart') }}" role="button" >
                                Buy Now!
                            </a>
                        </div>

                        <div class="item">
                                                        <span>
                                                            <a href="{{ url('/view-product') }}">
                                                                <img class="carasoul_image" src="{{ asset('/') }}front/images/img1.png">
                                                            </a></span>
                            <a class="btn btn-default"  href="{{ url('/cart') }}" role="button" >
                                Buy Now!
                            </a>
                        </div>
                        <div class="item">
                                                        <span>
                                                            <a href="{{ url('/view-product') }}">
                                                                <img class="carasoul_image" src="{{ asset('/') }}front/images/d2.jpg">
                                                            </a></span>
                            <a class="btn btn-default"  href="{{ url('/cart') }}" role="button" >
                                Buy Now!
                            </a>
                        </div>
                        <div class="item">
                                                        <span>
                                                            <a href="{{ url('/view-product') }}">
                                                                <img class="carasoul_image" src="{{ asset('/') }}front/images/d7.jpg">
                                                            </a></span>
                            <a class="btn btn-default"  href="{{ url('/cart') }}" role="button" >
                                Buy Now!
                            </a>
                        </div>
                    </div>

                    <script>
                        $(document).ready(function () {
                            $('.owl-carousel').owlCarousel({
                                loop: true,
                                margin: 10,
                                responsiveClass: true,
                                responsive: {
                                    0: {
                                        items: 2,
                                        nav: true
                                    },
                                    600: {
                                        items: 3,
                                        nav: false
                                    },
                                    1000: {
                                        items: 5,
                                        nav: true,
                                        loop: false,
                                        margin: 20
                                    }

                                }
                            })
                        })
                    </script>
                </div>
                <!--                                    </div>-->
            </div>
        </div>
    </div>
    <!----slidder End-!-->
    <!----content_1--!-->
    <div class="contentText Static">
        <h1>What is Lorem Ipsum?</h1>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s </p>
        <p>Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
    </div>
    <!----content_1 End--!-->


    <!----content_2 For New Products--!-->
    <div class="contentText">
        <h1>New Products For March</h1>
        <div class="row margin-top product-layout_width">
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Lorem Ipsum</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/d1.jpg" class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€134.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Lorem second</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/img4.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€21.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Lorem BIG-PACK</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/img6.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€159.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Officiis phaedrum</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/img13.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€26.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Et vel atqui putent, eum ad quidam adipiscing inciderint
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Munere vulputate</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/img14.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€120.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        At affert congue mea, ea est tritani tacimates petentium
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Lorem ipsum dolor</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/img15.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€199.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Professional context</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/img16.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€126.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        In a professional context it often happens that private
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Dolorem ipsum</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/img17.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€119.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Cicero famously</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/img1.png"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€171.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Sed ut perspiciatis, unde omnis iste natus error sit voluptatem
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----content_2 End--!-->

    <!----content_3--!-->
    <div class="contentText">
        <h1>Specials</h1>
        <div class="row margin-top product-layout_width">
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Aroma Therapy</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/d17.jpg" class="img-responsive" width="200" height="200"/>
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€134.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Herbal</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/d21.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€21.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Pills Drug</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/d23.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€235.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Capsule Pill</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/d2.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€137.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">Medication Cure</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/d7.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€212.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-layout  col-md-4 col-sm-6 col-xs-12">
                <div class="product-thumb-height">
                    <div class="product-thumb transition">
                        <ul>
                            <li class="li_product_title">
                                <div class="product_title">
                                    <a href="{{ url('/view-product') }}">China GuangDong Seeds</a>
                                </div></li>
                            <li class="li_product_image">
                                <div class="image">
                                    <a href="{{ url('/view-product') }}">
                                        <img src="{{ asset('/') }}front/images/d15.jpg"  class="img-responsive" width="200" height="200" />
                                    </a>

                                </div>
                            </li>
                            <li class="li_product_price">
                                <span class="old_price1"></span>
                                <span class="new_price1">€129.00</span>
                                <span class="saving1"></span><li>
                            <li class="li_product_desc">
                                <div class="caption">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing
                                    </p>
                                </div>
                            </li>
                            <li class="li_product_buy_button">
                                <a class="btn btn-default" id="but" href="{{ url('/cart') }}" role="button" >
                                    Buy Now!
                                </a>
                                <div class="pull-right">
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-heart"></i></button>
                                    <button  type="button" class="btn btn-primary wish_button"><i class="fa fa-exchange"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----content_3 End--!-->
    @endsection
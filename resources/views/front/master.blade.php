<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="icon" href="{{ asset('/') }}front/images/favicon.png"/>
    <title>@yield("title")</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/') }}front/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/') }}front/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/') }}front/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="{{ asset('/') }}front/css/owl-carousel.css"/>
    <script src="{{ asset('/') }}front/js/jquery.min.js"></script>
    <script src="{{ asset('/') }}front/js/owl-carousel.js"></script>
    <script src="{{ asset('/') }}front/js/bootstrap.min.js"></script>
    <script src="{{ asset('/') }}front/js/custom.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-4" id="logo" >
            <a href="{{ url('/') }}" class="logo-text">
                E-<span style="color:#39BAF0; font-size:40px">MEDICINE</span>
            </a>
        </div>
        <div class="col-md-2 col-sm-12 col-xs-12" style="display:none " id="navbar_hide" >
            <nav  role="navigation" class="navbar navbar-inverse">
                <a href="{{ url('/') }}" style="float: left" class="logo-text">
                    Medi<span style="color:#39BAF0; font-size:40px">STORE</span>
                </a>
                <div id="nav">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="background: #8EBE08; border: none; margin-right: 0">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav site_nav_menu1"  >
                            <li class="first " ><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Guarantee</a></li>
                            <li><a href="#">Disclaimer</a></li>
                            <li><a href="#">Shipping & Payment</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Site Map</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="col-md-4 col-md-offset-4 col-sm-offset-2  col-sm-6 col-xs-12" >
            <div id="top_right">
                <div id="cart">
                    <div class="text">
                        <div class="img">
                            <a href="cart.html"> <img class="img-responsive" src="{{ asset('/') }}front/images/cart.png" alt="" title="" width="26" height="27" /></a>
                        </div><span>Your cart:</span><span class="cart_total">€0.00</span><span class="cart_items">(0 items)</span>
                    </div>
                </div>
                <div id="bottom_right">
                    <div class="row">
                        <div class="col-md-6 col-xs-6 wd_auto">
                            <div class="left">
                                <div class="login">
                                    <a class="btn btn-default reg_button" href="{{ url('/login') }}">Login</a>
                                    <a class="btn btn-default reg_button" href="{{ url('/sign-up') }}">Signup</a>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown-bn wd-33 col-md-6 remove_PL col-xs-6">
                            <div class="row">
                                <div class=" pl-0 col-md-6 col-xs-6">

                                    <div class="dropdown btn-group">
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">language
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">English</a></li>
                                            <li><a href="#">French</a></li>
                                            <li><a href="#">German</a></li>
                                            <li><a href="#">Dutch</a></li>
                                            <li><a href="#">Swedish</a></li>
                                            <li><a href="#">Danish</a></li>
                                            <li><a href="#">Portuguese</a></li>
                                            <li><a href="#">Finish</a></li>
                                            <li><a href="#">German</a></li><li><a href="#">Norwegian</a></li>

                                        </ul>
                                    </div>
                                </div>

                                <div class="pl_0 col-md-6 col-xs-6">
                                    <div class="dropdown btn-group">
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">currency
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">U.S. Dollar</a></li>
                                            <li><a href="#">Euro</a></li>
                                            <li><a href="#">Pounds Sterling</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container-fluid bg-color">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <nav  role="navigation" class="navbar navbar-inverse" id="nav_show">
                        <div id="nav">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav site_nav_menu1"  >
                                    <li class="first "><a href="{{ url('/home') }}">Home</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Guarantee</a></li>
                                    <li><a href="#">Disclaimer</a></li>
                                    <li><a href="#">Shipping & Payment</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Terms & Conditions</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Site Map</a></li>
                                </ul>

                            </div>
                        </div>
                    </nav>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container" >
    <div class="row" id="search_manu" style="margin-top: 10px">
        <div class="col-md-6 col-xs-12">
            <form  name="quick_find">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" placeholder="Enter search keywords here" class="form-control input-lg" id="inputGroup"/>
                        <span class="input-group-addon">
                                    <a href="#" style="color:white">Search</a>
                                </span>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6 col-xs-12">

            <form  name="manufacturers">
                <div class="form-group">
                    <div class="">
                        <select  style="font-size: 14px; background: #EAEAEA; border: none;" name="manufacturers_id"  size="1" class="input-lg form-control arrow-hide date_class">
                            <option value="" selected="selected">Please Select manufacturers</option>
                            <option>lorem</option>
                            <option>lorem</option>
                            <option>lorem</option>
                            <option>lorem</option>
                        </select>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="site_content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12 left_sidebar1">
                <div id="left_part">
                    <div class="bs-example">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="infoBoxHeading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Categories</a>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <i  id="accordan_plus" class="indicator glyphicon glyphicon-chevron-down  pull-right"></i>
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="infoBoxContents">
                                            <a href="product.html">Category One</a>&nbsp;(94)<br />
                                            <a href="product.html">Category Two</a>&nbsp;(9)<br />
                                            <a href="product.html">Category Three</a>&nbsp;(5)<br />
                                            <a href="product.html">Category Four</a>&nbsp;(6)<br />
                                            <a href="product.html">Category Five</a>&nbsp;(94)<br />
                                            <a href="product.html">Category Six</a>&nbsp;(94)<br />
                                            <a href="product.html">Category Seven</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="infoBoxHeading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">What's New?</a>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                            <i id="accordan_plus" class="indicator glyphicon glyphicon-chevron-up  pull-right accordan_plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="infoBoxContainer">
                                            <div class="infoBoxHeading">
                                                <a href="#">What's New?</a>
                                            </div>
                                            <div class="infoBoxContents" id="sidebar">
                                                <div>
                                                    <a href="single-product.html">
                                                        <img src="{{ asset('/') }}front/images/img4.jpg" class="img-responsive" />
                                                    </a>
                                                </div>
                                                <a href="single-product.html">Lorem Simply</a><br />€21.00
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="infoBoxHeading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Information</a>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            <i id="accordan_plus" class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="infoBoxContents">
                                            <a href="#">Shipping &amp; Returns</a><br />
                                            <a href="#">Privacy Notice</a><br />
                                            <a href="#">Conditions of Use</a><br />
                                            <a href="#">Contact Us</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="infoBoxHeading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Bestsellers</a>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                            <i id="accordan_plus" class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="infoBoxContents" id="sidebar">
                                            <a href="single-product.html">
                                                <img src="{{ asset('/') }}front/images/img4.jpg"  class="img-responsive" />
                                            </a>
                                            <a href="single-product.html">Lorem Big Block</a><br /><del></del>
                                            <span class="productSpecialPrice">€21.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="infoBoxHeading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Specials</a>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                            <i id="accordan_plus" class="indicator glyphicon glyphicon-chevron-up  pull-right"></i>
                                        </a>
                                    </div>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <div class="infoBoxContents" id="sidebar">

                                            <a href="single-product.html">
                                                <img src="{{ asset('/') }}front/images/img6.jpg"  class="img-responsive" />
                                            </a>
                                            <a href="single-product.html">Lorem Big Block</a><br /><del></del>
                                            <span class="productSpecialPrice">€21.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <script>
                    function toggleChevron(e) {
                        $(e.target)
                            .prev('.panel-heading')
                            .find("i.indicator")
                            .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
                    }
                    $('#accordion').on('hidden.bs.collapse', toggleChevron);
                    $('#accordion').on('shown.bs.collapse', toggleChevron);
                </script>

            </div>
            <div class="col-md-9 col-sm-8 col-xs-12 right_sidebar1">
                <div id="right_part">
                    <div class="contentContainer">

            @yield('body')

                    </div>

                </div>

            </div>
        </div>

    </div>

</div>
<div id="footer1">
    <div class="container-fluid footer-background">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-3 col-xs-12 txt-center">
                        <a href="{{ url('/home') }}">
                            <span class="logo-text">E-MEDICINE</span>
                        </a>
                    </div>
                    <div class="col-md-7 col-sm-6 col-xs-12">
                        <div id="footer_menu">
                            <a href="{{ url('/home') }}">Home</a> |
                            <a href="#">About Us</a> |
                            <a href="#">Disclaimer</a> |
                            <a href="#">Guarantee</a> |
                            <a href="#">Shipping & Payment</a> |
                            <a href="#">Privacy Policy</a> <br class="disable_content" />
                            <a href="#">Terms & Conditions</a> |
                            <a href="#">Contact Us</a> |
                            <a href="#">Site Map<span></span></a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div id="social_icons" class="pull-right">
                            <a href="#" class="btn btn-default reg_button"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="btn btn-default reg_button"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="btn btn-default reg_button"><i class="fa fa-yahoo"></i></a>
                            <a href="#" class="btn btn-default reg_button"><i class="fa fa-envelope"></i></a>
                            <a href="#" class="btn btn-default reg_button"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="copyright">
                            © 2018 All right reserved. Designed by
                            <a href="http://www.facebook.com/sayedulsayem" target="_blank">Sayedul Sayem</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<a style="display: none" href="javascript:void(0);" class="scrollTop back-to-top" id="back-to-top">
    <i class="fa fa-chevron-up"></i>
</a>
</body>
</html>
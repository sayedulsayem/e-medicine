@extends("front.master")

@section('title')
    Sign Up
    @endsection

@section('body')
    <div class="col-sm-9" id="content">            <div class="breadcrumbs">
            <a href="{{ url('/home') }}"><i class="fa fa-home"></i></a>
            <a href="#">Register</a>
        </div>
        <h1>My Account Information</h1>
        <p> <small><strong class="define_note"></strong></small>If you already have an account with us, please login at the
            <a href="{{ url('/login') }}">login page</a>.</p>
        <form class="form-horizontal" action="{{ url('sign-up-store') }}" method="post">
            @csrf
            <div class="contentText">

                <fieldset id="account">
                    <h1>Your Personal Details</h1>
                    <div class="form-group required">
                        <label for="name" class="col-sm-2 control-label">Full Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" placeholder="Full Name" value="" name="name">
                        </div>
                    </div>

                    <div class="form-group required">
                        <label for="email" class="col-sm-2 control-label">E-Mail</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" placeholder="E-Mail" value="" name="email">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label for="mobile" class="col-sm-2 control-label">Mobile No</label>
                        <div class="col-sm-10">
                            <input type="tel" class="form-control" id="mobile" placeholder="Mobile No" value="" name="mobile">
                        </div>
                    </div>
                </fieldset>

                <fieldset id="address">
                    <h1>Your Address</h1>
                    <div class="form-group">
                        <label for="shop" class="col-sm-2 control-label">Shop Name (if any)</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="shop" placeholder="Shop Name" value="" name="shop">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label for="house" class="col-sm-2 control-label"> House No </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="house" placeholder="House No" value="" name="house">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label for="road" class="col-sm-2 control-label"> Road Name/No </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="road" placeholder="Road Name/No" value="" name="road">
                        </div>
                    </div>

                    <div class="form-group required">
                        <label for="sub_dist" class="col-sm-2 control-label">Sub District</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="sub-dist" placeholder="Sub District" value="" name="sub_dist">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label for="dist" class="col-sm-2 control-label">District</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="dist" placeholder="District" value="" name="dist">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label for="zip_code" class="col-sm-2 control-label">Zip Code</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="zip_code" placeholder="Zip Code" value="" name="zip_code">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label for="country" class="col-sm-2 control-label">Country</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="country" name="country">
                                <option value=""> --- Please Select --- </option>
                                <option value="bangladesh">Bangladesh</option>
                                <option value="india">India</option>
                                <option value="1">Afghanistan</option>
                                <option value="2">Albania</option>
                                <option value="3">Algeria</option>
                                <option value="4">American Samoa</option>
                                <option value="5">Andorra</option>
                                <option value="6">Angola</option>
                                <option value="7">Anguilla</option>
                                <option value="8">Antarctica</option>
                                <option value="9">Antigua and Barbuda</option>
                                <option value="10">Argentina</option>

                            </select>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <h1>Your Password</h1>
                    <div class="form-group required">
                        <label for="password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" placeholder="Password" value="" name="password">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label for="input-confirm" class="col-sm-2 control-label">Password Confirm</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="input-confirm" placeholder="Password Confirm" value="" name="confirm">
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <h1>Sign Up as</h1>
                    <div class="form-group required">
                        <label for="user_type" class="col-sm-2 control-label">User Type</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="user_type" name="user_type">
                                <option value=""> --- Please Select --- </option>
                                <option value="customer">Customer</option>
                                <option value="seller">Seller</option>
                            </select>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <h1>Newsletter</h1>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Subscribe</label>
                        <div class="col-sm-10">
                            <label class="radio-inline">
                                <input type="radio" value="1" name="newsletter">
                                Yes</label>
                            <label class="radio-inline">
                                <input type="radio" checked="checked" value="0" name="newsletter">
                                No</label>
                        </div>
                    </div>
                </fieldset>
                <div class="buttons">
                    <div class="pull-right">I have read and agree to the <a class="agree" href="#"><b>Privacy Policy</b></a>                                                        <input type="checkbox" value="1" name="agree">
                        &nbsp;
                        <input type="submit" class="btn btn-primary reg_button" value="Continue" >
                    </div>
                </div>
            </div>
        </form>
    </div>
    @endsection
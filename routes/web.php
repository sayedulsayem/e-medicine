<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/home','HomeController@showHome');

Route::get('/login','HomeController@showLogin');
Route::get('/sign-up','HomeController@showSignUp');

Route::get('/product','HomeController@showProduct');
Route::get('/product-list','HomeController@showProductList');
Route::get('/view-product','HomeController@showSingleProduct');
Route::get('/cart','HomeController@showCart');
Route::get('/checkout','HomeController@showCheckOut');

Route::get('/sellers','SellerController@showPanel');

Route::get('/customers','CustomerController@showPanel');

Route::post('/sign-up-store','UserController@signUpStore');
Route::post('/login-verify','UserController@loginVerify');

Route::get('/log-out','UserController@logOut');

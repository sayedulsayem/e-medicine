<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $fillable = [
        'user_id', 'shop', 'phone', 'house_no', 'road', 'sub_dis', 'dist', 'zip_code', 'country', 'verified'
    ];
}

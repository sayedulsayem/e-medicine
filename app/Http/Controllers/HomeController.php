<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    function showHome(){
        return view('index');
    }
    function showLogin(){
        return view('front.login');
    }
    function showSignUp(){
        return view('front.sign-up');
    }
    function showProduct(){
        return view('front.pages.product');
    }
    function showProductList(){
        return view('front.pages.product-list');
    }
    function showSingleProduct(){
        return view('front.pages.single-product');
    }
    function showCart(){
        return view('front.pages.cart');
    }
    function showCheckOut(){
        return view('front.pages.checkout');
    }
}

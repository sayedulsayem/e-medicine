<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Seller;
use App\User;
use Session;
use Illuminate\Http\Request;

class UserController extends Controller
{
    function signUpStore(Request $request){

        $pass=$request->password;

        $pass=md5($pass);

        $user=new User();
        $seller=new Seller();
        $customer=new Customer();

        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=$pass;
        $user->user_type=$request->user_type;
        $user->save();

        //return "saved in db user";

        if ($request->user_type == 'seller'){
            $seller->user_id=$user->id;
            $seller->shop=$request->shop;
            $seller->mobile=$request->mobile;
            $seller->house=$request->house;
            $seller->road=$request->road;
            $seller->sub_dist=$request->sub_dist;
            $seller->dist=$request->dist;
            $seller->zip_code=$request->zip_code;
            $seller->country=$request->country;
            $seller->save();

            Session::put("id",$user->id);
            Session::put("name",$user->name);
            Session::put("email",$user->email);
            Session::put("user_type",$user->user_type);

            return redirect('/sellers')->with('msg',"you have successfully registered");

        }

        if ($request->user_type == 'customer'){
            $customer->user_id=$user->id;
            $customer->mobile=$request->mobile;
            $customer->house=$request->house;
            $customer->road=$request->road;
            $customer->sub_dist=$request->sub_dist;
            $customer->dist=$request->dist;
            $customer->zip_code=$request->zip_code;
            $customer->country=$request->country;
            $customer->save();

            Session::put("id",$user->id);
            Session::put("name",$user->name);
            Session::put("email",$user->email);
            Session::put("user_type",$user->user_type);

            return redirect('/customers')->with('msg',"you have successfully registered");
        }

    }

    function loginVerify(Request $request){

        $pass=$request->password;
        $pass=md5($pass);

        $userById=User::where('email',$request->email)->first();

        if (isset($userById)){
            if ($userById->email == $request->email){
                if ($userById->password == $pass){

                    Session::put('id',$userById->id);
                    Session::put('name',$userById->name);
                    Session::put('email',$userById->email);
                    Session::put('user_type',$userById->user_type);

                    if ($userById->user_type == 'seller'){
                        return redirect('/sellers')->with('msg','logged in successful');
                    }elseif ($userById->user_type == 'customer'){
                        return redirect('/customers')->with('msg','logged in successful');
                    }
                }
                else{
                    return redirect('/login')->with('msg','user name password not matched');
                }
            }
            else{
                return redirect('/login')->with('msg','user name password not matched');
            }
        }
        return redirect('/login')->with('msg','user name password not matched');
    }

    function logOut(){
        Session::flush();
        return redirect('/login');
    }
}
